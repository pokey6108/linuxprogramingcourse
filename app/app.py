from flask import Flask
import werkzeug
from routes import main

app = Flask(__name__)
app.register_blueprint(main)
