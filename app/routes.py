from flask import Blueprint, render_template
from markupsafe import escape
main = Blueprint('main', __name__)

@main.route('/')
@main.route('/<cource_id>')
def index(cource_id=0):
    try:
        render = render_template(f"course_{escape(cource_id)}.html")
        return render
    except:
        return 'something error'

@main.route('/md/<md_id>')
def get_markdown(md_id):
    return md_id
