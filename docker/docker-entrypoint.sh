#!/bin/bash

echo $VOLUME_DIR
. ~/.profile
pip3 install -r $VOLUME_DIR # please change to your volume dir path manually.

echo $(pwd)

export FLASK_APP=app.py
flask run --host=0.0.0.0
